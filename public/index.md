<img src="elixir-norway.png" width="150">  <img src="nmbu.png" width="200"> 

## Workshop on using High-Performance Cluster and ELIXIR tools for Bioinformatics 

###[Program](program)

###Morning session

- Computer setup

- Slides

###Afternoon session

- Introduction to NeLS and Galaxy

- [Galaxy workflow](galaxy-workflow)

